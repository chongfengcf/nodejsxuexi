//全部引入
import * as mod1 from './mod1.js';
console.log(mod1);

alert(mod1.sum(mod1.a,mod1.b));

let p = new mod1.Person('blue', 18);
p.show();

//引入某些模块
import {a,b,c} from './mod1';
console.log(a,b,c);

//引入默认
import s from './mod1';
alert(s);

//异步加载
//编译后生成多个文件
import('./mod2').then(mod2=>{
    alert(`mod2的qq:${mod2.qq}`);
}, err=>{
    alert('mod2加载失败');
})

