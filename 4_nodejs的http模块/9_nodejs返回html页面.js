const http = require('http');
const fs = require('fs');

let server = http.createServer(function (req, res) {
    console.log(req.url);
    //req.url => '/1.html'
    //=>'www/1.html'

    fs.readFile(`www${req.url}`, (err, buffer)=> {
        if(err) {
            res.writeHead(404);
            res.write('Not Found');
            res.end();
            console.log(err);
        } else {
            res.write(buffer);
            res.end();
        }
    });
});

server.listen(8080);